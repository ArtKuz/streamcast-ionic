# Streamcast
[Generator-M-Ionic v1.9.2](https://github.com/mwaylabs/generator-m-ionic)  


## Установка
`npm install --global yo`  
`npm install --global gulp`  
`npm install --global bower`  
`npm install --global generator-m-ionic`


## Сборка проекта
`gulp watch` - запустить текущий код приложения в браузере в новой вкладке.  
`gulp watch --no-open` - запустить текущий код приложения в браузере, не открывая новой вкладки.  

## Работа с Cordova
`gulp --cordova 'plugins ls'` - список установленных плагинов.  
`gulp --cordova 'plugin add plugin_name --save'` - установка плагина.  
`gulp --cordova 'plugins rm plugin_name ---save'` - удаление плагина.  

`gulp --cordova 'platform ls'` - список платформ.
`gulp --cordova 'platform add android --save'` - добаввить платформу.  
`gulp --cordova 'platform rm android --save'` - удалить платформу.  

`gulp --cordova 'prepare'` - установка Cordova платформ и плагинов.

`gulp --cordova 'run android --device'` - запуск приложения на устройстве Android.  
`gulp --cordova 'run ios --device'` - запуск приложения на устройстве iOS.  
`gulp --cordova 'run android' --no-build` - запуск приложения с последней сборкой.  

`gulp build` - сборка проекта в папку *www*.  
`gulp build --force-build` - сборка, даже если есть ошибки в коде и синтаксисе.  
`gulp build --minify` - сборка с минификацией javascript, CSS, HTML и images.
 
 `gulp --cordova 'run android --device' --force-build --minify` - продакшен сборка.  
 `gulp --cordova 'run android --device' --minify` - продакшен сборка.  

 `gulp watch-build` - открыть сборку в браузере в новой вкладке.  
 `gulp watch-build --no-open` - собрать без открытия новой вкладки в браузере.  
 `gulp watch-build --no-build` - собрать минифицированную версию.  


## Генерация модулей и компонентов
`yo m-ionic:module <moduleName>`  
```bash
yo m-ionic:<component> <name> <moduleName>
# <components>
#   constant
#   controller
#   directive
#   filter
#   pair - template and controller
#   template
#   service
```

## Тесты
`gulp test`  
`gulp protractor`  
