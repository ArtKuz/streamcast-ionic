'use strict';

angular
  .module('componentItemRoom')
  .directive('itemRoom', function () {
    return {
      restrict: 'E',
      templateUrl: 'component-item-room/templates/item-room.html',
      controllerAs: 'vm',
      scope: {
        room: '='
      },
      bindToController: true,
      controller: function($state) {
        var vm = this;

        vm.onRoom = function() {
          if (vm.room.group) {
            $state.go('main.group-chat.chat', { group_chat_id: vm.room.id });
          } else {
            $state.go('main.chat', { chat_id: vm.room.id });
          }
        };
      }
    };
  });
