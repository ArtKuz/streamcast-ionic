'use strict';

angular
  .module('componentMoney', [
    'pascalprecht.translate'
  ])
  .config(function ($translateProvider, $translatePartialLoaderProvider) {
    $translatePartialLoaderProvider.addPart('component-money');
  });
