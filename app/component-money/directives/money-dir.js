'use strict';

angular
  .module('componentMoney')
  .directive('money', function () {
    return {
      restrict: 'E',
      templateUrl: 'component-money/templates/money-menu.html',
      controllerAs: 'money',
      scope: {
        user: '='
      },
      bindToController: true,
      controller: function($scope,
                           $mdDialog) {
        var money = this;

        money.showDialog = function($event) {
          $mdDialog.show({
            parent: angular.element(document.body),
            targetEvent: $event,
            templateUrl: 'component-money/templates/money-buy-dialog.html',
            locals: {},
            controller: DialogController
          }).then(function () {}, function () {});

          function DialogController($scope, $mdDialog) {
            $scope.coinsArr = [10, 30, 50, 100];
            $scope.priceArr = [24.99, 78.45, 117.68, 209.20];

            $scope.closeDialog = function() {
              $mdDialog.cancel();
            };

            $scope.okDialog = function(selectedCoins) {
              money.user.money += selectedCoins;
              $mdDialog.hide();
            };
          }
        }
      }
    };
  });
