'use strict';

angular
  .module('componentItemUserCheckbox')
  .directive('itemUserCheckbox', function () {
    return {
      restrict: 'E',
      templateUrl: 'component-item-user-checkbox/templates/item-user-checkbox.html',
      controllerAs: 'vm',
      scope: {
        user: '='
      },
      bindToController: true,
      controller: function() {
        var vm = this;
      }
    };
  });
