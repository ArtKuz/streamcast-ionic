'use strict';

angular
  .module('screenTabShopBackgrounds', [
    'componentItemShop',
    'componentBtnSearch'
  ])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.shop.backgrounds', {
        url: "/backgrounds",
        views: {
          'tab-shop-backgrounds': {
            templateUrl: "screen-tab-shop-backgrounds/templates/tab-shop-backgrpunds.html",
            controller: 'TabShopBackgrpundsCtrl as vm'
          }
        }
      })
  });
