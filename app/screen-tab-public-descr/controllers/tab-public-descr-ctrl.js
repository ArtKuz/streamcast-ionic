'use strict';

angular
  .module('screenTabPublicDescr')
  .controller('TabPublicDescrCtrl', function (Mocks) {
    var vm = this;

    vm.public = Mocks.publics.populars[0];
  });
