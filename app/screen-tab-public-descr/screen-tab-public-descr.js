'use strict';

angular
  .module('screenTabPublicDescr', [
    'componentBtnSearch',
    'componentBtnMoreSettings'
  ])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.public.descr', {
        url: "/descr",
        views: {
          'tab-public-descr': {
            templateUrl: "screen-tab-public-descr/templates/tab-public-descr.html",
            controller: 'TabPublicDescrCtrl as vm'
          }
        }
      })
  });
