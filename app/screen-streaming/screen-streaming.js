'use strict';

angular
  .module('screenStreaming', [])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.sreaming', {
        url: '/sreaming',
        views: {
          'pageContent': {
            templateUrl: 'screen-streaming/templates/streaming.html',
            controller: 'StreamingCtrl as vm'
          }
        }
      });
  });
