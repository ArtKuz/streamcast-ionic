'use strict';

angular
  .module('screenStreaming')
  .controller('StreamingCtrl', function () {
    var vm = this;

    vm.showSettings = true;
    vm.showSettingsChanel = false;

    vm.clickSetting = function(channel) {
      vm.showSettings = !vm.showSettings;
      vm.showSettingsChanel = !vm.showSettingsChanel;
      if (channel === 'youtube') {
        vm.channelImg = 'screen-streaming/assets/images/youtube.png';
      } else {
        vm.channelImg = 'screen-streaming/assets/images/twitch.png';
      }
    }

  });
