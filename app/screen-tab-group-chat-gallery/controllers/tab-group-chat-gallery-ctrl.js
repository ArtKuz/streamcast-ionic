
'use strict';

angular
  .module('screenTabGroupChatGallery')
  .controller('TabGroupChatGalleryCtrl', function (Mocks) {
    var vm = this;

    vm.chat = Mocks.chat[1];

    vm.gallery = [
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_01.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_02.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_03.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_04.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_05.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_06.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_01.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_02.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_03.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_04.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_05.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_06.png'
      }
    ];
  });
