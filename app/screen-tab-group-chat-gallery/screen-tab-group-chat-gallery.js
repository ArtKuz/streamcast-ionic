'use strict';

angular
  .module('screenTabGroupChatGallery', [
    'componentMessage',
    'componentBtnClip',
    'componentSendMessage',
    'componentSendSticker',
    'componentBtnMoreSettings',
    'componentBtnAddUser',
    'componentGallery'
  ])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.group-chat.gallery', {
        url: "/gallery",
        views: {
          'tab-group-chat-gallery': {
            templateUrl: "screen-tab-group-chat-gallery/templates/tab-group-chat-gallery.html",
            controller: 'TabGroupChatGalleryCtrl as vm'
          }
        }
      })
  });
