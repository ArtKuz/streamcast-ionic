'use strict';

angular
  .module('main', [
    'componentProfile',
    'componentItemMenu',
    'componentMoney',
    'serviceMocks'
  ])
  .config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/main/tabs/news');
    $stateProvider
      .state('main', {
        url: '/main',
        abstract: true,
        templateUrl: 'main/templates/main.html',
        controller: 'MainCtrl as menu'
      })
  });
