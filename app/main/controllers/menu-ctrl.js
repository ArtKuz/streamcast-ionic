'use strict';

angular
  .module('main')
  .controller('MainCtrl', function ($scope,
                                    $mdUtil,
                                    $mdSidenav,
                                    Mocks) {
    var menu = this;

    menu.toggleLeft = buildToggler('left');

    menu.closeSideNav = function() {
      $mdSidenav('left').close();
    };

    function buildToggler(navID) {
      return $mdUtil.debounce(function() {
        $mdSidenav(navID).toggle();
      }, 0);
    }

    $scope.$on('menu:close', function () {
      menu.closeSideNav();
    });

    menu.isAndroid = ionic.Platform.isAndroid();

    menu.profileId = {
      user_id: 0
    };

    /**
     * mocks
     */
    menu.user = Mocks.user;

  });
