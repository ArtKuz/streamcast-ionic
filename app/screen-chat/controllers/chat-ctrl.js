'use strict';

angular
  .module('screenChat')
  .controller('ChatCtrl', function (Mocks,
                                    $timeout,
                                    $state,
                                    $ionicScrollDelegate,
                                    $scope) {
    var vm = this;

    vm.chat = Mocks.chat[0];

    $timeout(function () {
      $ionicScrollDelegate.scrollBottom(true);
    }, 300);

    $scope.$on('sticker:messages', function() {
      vm.stickers = !vm.stickers;
    });

    vm.onBack = function() {
      $state.go('main.tabs.rooms');
    }
  });
