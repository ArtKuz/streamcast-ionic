'use strict';

angular
  .module('screenChat', [
    'serviceMocks',
    'componentMessage',
    'componentSendMessage',
    'componentBtnCreateGroupChat',
    'componentBtnClip'
  ])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.chat', {
        url: '/chat/:chat_id',
        views: {
          'pageContent': {
            templateUrl: "screen-chat/templates/chat.html",
            controller: 'ChatCtrl as vm'
          }
        }
      })
  });

