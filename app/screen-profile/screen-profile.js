'use strict';

angular
  .module('screenProfile', [
    'pascalprecht.translate',
    'componentProfile',
    'componentBtnChatUser',
    'componentBtnReply'
  ])
  .config(function ($translateProvider, $translatePartialLoaderProvider) {
    $translatePartialLoaderProvider.addPart('screen-profile');
  })
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.profile', {
        url: '/profile/:user_id',
        views: {
          'pageContent': {
            templateUrl: 'screen-profile/templates/profile.html',
            controller: 'ProfileCtrl as vm'
          }
        }
      });
  });
