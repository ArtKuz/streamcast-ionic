'use strict';

angular
  .module('screenProfile')
  .controller('ProfileCtrl', function (Mocks,
                                       $scope,
                                       $state,
                                       $stateParams,
                                       $ionicNavBarDelegate,
                                       $timeout) {
    var vm = this;

    vm.params = {
      user_id: Number($stateParams.user_id)
    };

    if (Number($stateParams.user_id) !== 0) {
      vm.user = Mocks.user;
    }

    vm.onBack = function() {
      $state.go('main.tabs.rooms');
    };
  });
