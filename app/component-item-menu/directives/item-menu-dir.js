'use strict';

angular
  .module('componentItemMenu')
  .directive('itemMenu', function () {
    return {
      restrict: 'E',
      templateUrl: 'component-item-menu/templates/item-menu.html',
      controllerAs: 'vm',
      scope: {
        title: '@',
        state: '@',
        stateParams: '=',
        icon: '@',
        iconSvg: '@',
        borderTop: '='
      },
      bindToController: true,
      controller: function($rootScope,
                           $timeout,
                           $ionicHistory,
                           $state) {
        var vm = this;

        vm.navigateTo = function () {
          $rootScope.$broadcast('menu:close');
          $timeout(function () {
            if (vm.state && $ionicHistory.currentStateName() != vm.state) {
              $state.go(vm.state,  vm.stateParams);
            }
          }, 300);
        };
      }
    };
  });
