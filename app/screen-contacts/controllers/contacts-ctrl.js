'use strict';

angular
  .module('screenContacts')
  .controller('ContactsCtrl', function (Mocks) {
    var vm = this;

    vm.users = Mocks.users;
  });
