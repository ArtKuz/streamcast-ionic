'use strict';

angular
  .module('screenContacts', [
    'componentBtnSearch'
  ])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.contacts', {
        url: '/contacts',
        views: {
          'pageContent': {
            templateUrl: "screen-contacts/templates/contacts.html",
            controller: 'AddToGroupCtrl as vm'
          }
        }
      })
  });
