'use strict';

angular
  .module('screenTabPublicsMy', [
    'componentItemPublic',
    'componentBtnSearch'
  ])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.publics.my', {
        url: "/my",
        views: {
          'tab-publics-my': {
            templateUrl: "screen-tab-publics-my/templates/tab-publics-my.html",
            controller: 'TabPublicsMyCtrl as vm'
          }
        }
      })
  });
