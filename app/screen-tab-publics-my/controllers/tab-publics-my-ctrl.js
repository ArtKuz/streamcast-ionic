'use strict';

angular
  .module('screenTabPublicsMy')
  .controller('TabPublicsMyCtrl', function (Mocks) {
    var vm = this;

    vm.populars = Mocks.publics.my;
  });
