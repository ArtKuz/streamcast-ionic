'use strict';

var appPrimaryColor;

angular
  .module('Streamcast', [
    'ionic',
    'ngCordova',
    'ui.router',
    'pascalprecht.translate',
    'ngMaterial',
    'ngMdIcons',
    'main',
    'screenProfile',
    'screenTabsMain',
    'screenTabMainNews',
    'screenTabMainRooms',
    'screenChat',
    'screenAddToGroup',
    'screenTabsGroupChat',
    'screenTabGroupChatChat',
    'screenTabGroupChatWall',
    'screenTabGroupChatGallery',
    'screenTabGroupChatMembers',
    'screenGroupChatSettings',
    'screenTabsPublics',
    'screenTabPublicsPopular',
    'screenTabPublicsSubscription',
    'screenTabPublicsMy',
    'screenTabsPublic',
    'screenTabPublicPublics',
    'screenTabPublicGallery',
    'screenTabPublicDescr',
    'screenContacts',
    'screenContactsBlackList',
    'screenTabsShop',
    'screenTabShopBackgrounds',
    'screenTabShopStickers',
    'screenSettings',
    'screenStreaming'
  ])
  .config(function ($translateProvider) {
    $translateProvider
      .preferredLanguage('ru')
      .fallbackLanguage('en');

    $translateProvider.useSanitizeValueStrategy(null);
    $translateProvider.useLoader('$translatePartialLoader', {
      urlTemplate: '{part}/translations/{lang}.json'
    });
  })
  .run(function($rootScope,
                $timeout,
                $ionicNavBarDelegate) {
    $rootScope.$on('$stateChangeSuccess',
      function(event, toState, toParams, fromState, fromParams){
        $rootScope.$on('$ionicView.enter', function (event, viewData) {
          $timeout(function() {
            $ionicNavBarDelegate.align('left');
          });
        });
      })
  })
  .run(function ($ionicPlatform,
                 $rootScope,
                 $ionicHistory,
                 $state,
                 $mdDialog,
                 $mdBottomSheet) {

    function groupChatHeader() {
      return "" +
        ".bar .title {" +
          "overflow: visible" +
        "}" +
        ".bar-header.has-tabs-top.bar {" +
          "height: 154px;" +
          "background: url('screen-tabs-group-chat/assets/images/background.jpg') center center / 120% no-repeat !important;" +
        "}" +
        ".has-header {" +
          "top: 154px;" +
        "}" +
        ".tabs-top > .tabs, .tabs.tabs-top {" +
          "top: 154px;" +
        "}" +
        ".tabs-top .has-tabs-top {" +
          "top: 203px;" +
        "}" +
        ".platform-ios.platform-cordova:not(.fullscreen) .bar-header:not(.bar-subheader) {" +
          "height: 174px;" +
        "}" +
        ".platform-ios.platform-cordova:not(.fullscreen) .bar-subheader," +
        ".platform-ios.platform-cordova:not(.fullscreen) .has-header," +
        ".platform-ios.platform-cordova:not(.fullscreen) .tabs-top>.tabs," +
        ".platform-ios.platform-cordova:not(.fullscreen) .tabs.tabs-top {" +
          "top: 174px;" +
        "}" +
        ".platform-ios.platform-cordova:not(.fullscreen) .has-header.has-tabs-top {" +
          "top: 223px;" +
        "}"
    }

    function initialRootScope() {
      $rootScope.appPrimaryColor = appPrimaryColor;
      $rootScope.isAndroid = ionic.Platform.isAndroid();
      $rootScope.isIOS = ionic.Platform.isIOS();
    }

    function hideActionControl() {
      $mdBottomSheet.cancel();
      $mdDialog.cancel();
    }


    function createCustomStyle(stateName) {
      var customStyle =
        ".material-background {" +
        "   background-color          : " + appPrimaryColor + " !important;" +
        "   border-style              : none;" +
        "}" +
        ".spinner-android {" +
        "   stroke                    : " + appPrimaryColor + " !important;" +
        "}" +
        ".material-background-nav-bar { " +
        "   background-color        : " + appPrimaryColor + " !important; " +
        "   border-style            : none;" +
        "}" +
        ".md-primary-color {" +
        "   color                     : " + appPrimaryColor + " !important;" +
        "}";

      switch (stateName) {
        case "main.group-chat" :
        case "main.group-chat.chat":
        case "main.group-chat.wall":
        case "main.group-chat.gallery":
          customStyle += groupChatHeader();
          break;
      }
      return customStyle;
    }

    $rootScope.$on('$stateChangeSuccess',
      function(event, toState, toParams, fromState, fromParams){
        $rootScope.customStyle = createCustomStyle(toState.name);
      });

    $ionicPlatform.ready(function () {
      ionic.Platform.isFullScreen = true;
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }

      initialRootScope();

      $rootScope.$on('$ionicView.beforeEnter', function () {
        hideActionControl();
        $rootScope.customStyle = createCustomStyle($ionicHistory.currentStateName());
      });
    });
  })
  .config(function ($ionicConfigProvider,
                    $stateProvider,
                    $urlRouterProvider,
                    $mdThemingProvider,
                    $mdIconProvider,
                    $mdColorPalette) {

    $ionicConfigProvider.spinner.icon("android");
    $ionicConfigProvider.views.swipeBackEnabled(false);
    $ionicConfigProvider.backButton.text('');
    $ionicConfigProvider.backButton.icon('ion-android-arrow-back');
    $ionicConfigProvider.navBar.alignTitle('left');

    $mdThemingProvider
      .theme('default')
      .primaryPalette('green')
      .accentPalette('green', {
        'default': '500'
      });

    appPrimaryColor = $mdColorPalette[$mdThemingProvider._THEMES.default.colors.primary.name]["500"];

  });
