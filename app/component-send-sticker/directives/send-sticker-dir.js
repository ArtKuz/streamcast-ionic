'use strict';

angular
  .module('componentSendSticker')
  .directive('sendSticker', function () {
    return {
      restrict: 'E',
      templateUrl: 'component-send-sticker/templates/send-sticker.html',
      controllerAs: 'vm',
      scope: {},
      bindToController: true,
      controller: function() {
        var vm = this;

        vm.chooseCollection = 0;

        vm.onChooseCollection = function(id) {
          vm.chooseCollection = id;
        }

      }
    };
  });
