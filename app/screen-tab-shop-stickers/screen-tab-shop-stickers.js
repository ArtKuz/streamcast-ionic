'use strict';

angular
  .module('screenTabShopStickers', [
    'componentItemShop',
    'componentBtnSearch'
  ])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.shop.stickers', {
        url: "/stickers",
        views: {
          'tab-shop-stickers': {
            templateUrl: "screen-tab-shop-stickers/templates/tab-shop-stickers.html",
            controller: 'TabShopStickersCtrl as vm'
          }
        }
      })
  });
