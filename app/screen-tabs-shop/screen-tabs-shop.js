'use strict';

angular
  .module('screenTabsShop', [])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.shop', {
        url: '/shop',
        abstract: true,
        views: {
          'pageContent': {
            templateUrl: "screen-tabs-shop/templates/tabs-shop.html"
          }
        }
      })
  });
