'use strict';

angular
  .module('componentMessage')
  .directive('message', function () {
    return {
      restrict: 'E',
      templateUrl: 'component-message/templates/message.html',
      controllerAs: 'vm',
      scope: {
        message: '='
      },
      bindToController: true,
      controller: function() {
        var vm = this;
      }
    };
  });
