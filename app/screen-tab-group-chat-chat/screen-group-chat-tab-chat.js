'use strict';

angular
  .module('screenTabGroupChatChat', [
    'componentMessage',
    'componentBtnClip',
    'componentSendMessage',
    'componentSendSticker',
    'componentBtnMoreSettings',
    'componentBtnAddUser'
  ])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.group-chat.chat', {
        url: "/chat",
        views: {
          'tab-group-chat-chat': {
            templateUrl: "screen-tab-group-chat-chat/templates/tab-group-chat-chat.html",
            controller: 'TabGroupChatChatCtrl as vm'
          }
        }
      })
  });
