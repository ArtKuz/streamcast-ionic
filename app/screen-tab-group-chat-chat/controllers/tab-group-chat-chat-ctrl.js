'use strict';

angular
  .module('screenTabGroupChatChat')
  .controller('TabGroupChatChatCtrl', function (Mocks,
                                                $timeout,
                                                $ionicScrollDelegate,
                                                $scope) {
    var vm = this;

    vm.chat = Mocks.chat[1];

    $timeout(function () {
      $ionicScrollDelegate.scrollBottom();
    }, 300);

    $scope.$on('sticker:messages', function() {
      vm.stickers = !vm.stickers;
    });
  });
