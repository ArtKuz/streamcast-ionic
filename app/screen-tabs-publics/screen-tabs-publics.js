'use strict';

angular
  .module('screenTabsPublics', [])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.publics', {
        url: '/publics',
        abstract: true,
        views: {
          'pageContent': {
            templateUrl: "screen-tabs-publics/templates/tabs-publics.html"
          }
        }
      })
  });
