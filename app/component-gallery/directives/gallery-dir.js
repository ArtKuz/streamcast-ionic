'use strict';

angular
  .module('componentGallery')
  .directive('gallery', function () {
    return {
      restrict: 'E',
      templateUrl: 'component-gallery/templates/gallery.html',
      controllerAs: 'vm',
      scope: {
        images: '='
      },
      bindToController: true,
      controller: function($scope,
                           $ionicBackdrop,
                           $ionicModal,
                           $ionicSlideBoxDelegate,
                           $ionicScrollDelegate) {
        var vm = this;

        vm.zoomMin = 1;

        vm.showImages = function(index) {
          vm.activeSlide = index;
          vm.showModal('component-gallery/templates/modal-gallery.html');
        };

        vm.showModal = function(templateUrl) {
          $ionicModal.fromTemplateUrl(templateUrl, {
            scope: $scope
          }).then(function(modal) {
            vm.modal = modal;
            vm.modal.show();
          });
        };

        vm.closeModal = function() {
          vm.modal.hide();
          vm.modal.remove()
        };

        vm.updateSlideStatus = function(slide) {
          var zoomFactor = $ionicScrollDelegate.$getByHandle('scrollHandle' + slide).getScrollPosition().zoom;
          if (zoomFactor == vm.zoomMin) {
            $ionicSlideBoxDelegate.enableSlide(true);
          } else {
            $ionicSlideBoxDelegate.enableSlide(false);
          }
        };

      }
    };
  });
