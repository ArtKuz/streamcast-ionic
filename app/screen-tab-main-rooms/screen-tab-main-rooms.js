'use strict';

angular
  .module('screenTabMainRooms', [
    'serviceMocks',
    'componentItemRoom',
    'componentBtnSearch'
  ])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.tabs.rooms', {
        url: "/rooms",
        views: {
          'tab-rooms': {
            templateUrl: "screen-tab-main-rooms/templates/tab-main-rooms.html",
            controller: 'TabMainRoomsCtrl as vm'
          }
        }
      })
  });
