'use strict';

angular
  .module('screenTabMainRooms')
  .controller('TabMainRoomsCtrl', function (Mocks) {
    var vm = this;

    vm.roomsList = Mocks.rooms;
  });
