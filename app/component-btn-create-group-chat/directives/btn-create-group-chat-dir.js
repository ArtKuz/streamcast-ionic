'use strict';

angular
  .module('componentBtnCreateGroupChat')
  .directive('btnCreateGroupChat', function () {
    return {
      restrict: 'E',
      templateUrl: 'component-btn-create-group-chat/templates/btn-create-group-chat.html',
      controllerAs: 'vm',
      scope: {},
      bindToController: true,
      controller: function($mdDialog,
                           $state) {
        var vm = this;

        vm.showDialog = function($event) {
          $mdDialog.show({
            parent: angular.element(document.body),
            targetEvent: $event,
            templateUrl: 'component-btn-create-group-chat/templates/dialog-create-group-chat.html',
            locals: {},
            controller: DialogController
          }).then(function () {}, function () {});

          function DialogController($scope, $mdDialog) {
            $scope.closeDialog = function() {
              $mdDialog.cancel();
            };

            $scope.okDialog = function() {
              $state.go('main.add-to-group');
              $mdDialog.hide();
            };
          }
        }
      }
    };
  });
