'use strict';

angular
  .module('componentItemUserControl')
  .directive('itemUserControl', function () {
    return {
      restrict: 'E',
      templateUrl: 'component-item-user-control/templates/item-user-control.html',
      controllerAs: 'vm',
      scope: {
        user: '='
      },
      bindToController: true,
      controller: function() {
        var vm = this;

        vm.foo = function() {

        };
      }
    };
  });
