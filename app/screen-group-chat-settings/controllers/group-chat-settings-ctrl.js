'use strict';

angular
  .module('screenGroupChatSettings')
  .controller('GroupChatSettingsCtrl', function (Mocks) {
    var vm = this;

    vm.chat = Mocks.chat[1];
  });
