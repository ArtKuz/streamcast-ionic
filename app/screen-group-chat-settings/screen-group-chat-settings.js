'use strict';

angular
  .module('screenGroupChatSettings', [])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.group-chat-settings', {
        url: '/group-chat-settings/:group_chat_id',
        views: {
          'pageContent': {
            templateUrl: "screen-group-chat-settings/templates/group-chat-settings.html",
            controller: 'GroupChatSettingsCtrl as vm'
          }
        }
      })
  });
