'use strict';

angular
  .module('componentItemPublic')
  .directive('itemPublic', function () {
    return {
      restrict: 'E',
      templateUrl: 'component-item-public/templates/item-public.html',
      controllerAs: 'vm',
      scope: {
        public: '=',
        type: '@',
        id: '@'
      },
      bindToController: true,
      controller: function() {
        var vm = this;

      }
    };
  });
