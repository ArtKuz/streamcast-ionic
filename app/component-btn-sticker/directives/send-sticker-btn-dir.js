'use strict';

angular
  .module('componentBtnSticker')
  .directive('btnSticker', function () {
    return {
      restrict: 'E',
      templateUrl: 'component-btn-sticker/templates/btn-sticker.html',
      controllerAs: 'vm',
      scope: {},
      bindToController: true,
      controller: function() {
        var vm = this;
      }
    };
  });
