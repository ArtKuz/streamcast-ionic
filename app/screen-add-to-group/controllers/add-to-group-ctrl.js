'use strict';

angular
  .module('screenAddToGroup')
  .controller('AddToGroupCtrl', function (Mocks,
                                          $state) {
    var vm = this;

    vm.users = Mocks.users;

    vm.onBack = function() {
      $state.go('main.tabs.rooms');
    }
  });
