'use strict';

angular
  .module('screenAddToGroup', [
    'componentItemUserCheckbox',
    'componentBtnSearch'
  ])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.add-to-group', {
        url: '/add-to-group',
        views: {
          'pageContent': {
            templateUrl: "screen-add-to-group/templates/add-to-group.html",
            controller: 'AddToGroupCtrl as vm'
          }
        }
      })
  });
