'use strict';

angular
  .module('screenContactsBlackList')
  .controller('ContactsBlackListCtrl', function (Mocks) {
    var vm = this;

    vm.users = Mocks.users;
  });
