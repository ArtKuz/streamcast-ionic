'use strict';

angular
  .module('screenContactsBlackList', [
    'componentBtnSearch'
  ])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.contacts-black-list', {
        url: '/contacts-black-list',
        views: {
          'pageContent': {
            templateUrl: "screen-contacts-black-list/templates/contacts-black-list.html",
            controller: 'ContactsBlackListCtrl as vm'
          }
        }
      })
  });
