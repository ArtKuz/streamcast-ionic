'use strict';

angular
  .module('screenTabPublicGallery', [
    'componentBtnSearch',
    'componentBtnMoreSettings',
    'componentGallery'
  ])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.public.gallery', {
        url: "/gallery",
        views: {
          'tab-public-gallery': {
            templateUrl: "screen-tab-public-gallery/templates/tab-public-gallery.html",
            controller: 'TabPublicGalleryCtrl as vm'
          }
        }
      })
  });
