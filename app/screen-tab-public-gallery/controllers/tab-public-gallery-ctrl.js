'use strict';

angular
  .module('screenTabPublicGallery')
  .controller('TabPublicGalleryCtrl', function (Mocks) {
    var vm = this;

    vm.public = Mocks.publics.populars[0];

    vm.gallery = [
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_01.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_02.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_03.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_04.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_05.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_06.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_01.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_02.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_03.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_04.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_05.png'
      },
      {
        src:'screen-tab-group-chat-gallery/assets/images/gallery_06.png'
      }
    ];

  });
