'use strict';

angular
  .module('componentSendMessage')
  .directive('sendMessage', function () {
    return {
      restrict: 'E',
      templateUrl: 'component-send-message/templates/send-message.html',
      controllerAs: 'vm',
      scope: {},
      bindToController: true,
      controller: function($rootScope) {
        var vm = this;

        vm.chooseSticker = function() {
          $rootScope.$broadcast('sticker:messages');
        }
      }
    };
  });
