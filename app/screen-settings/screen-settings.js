'use strict';

angular
  .module('screenSettings', [])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.settings', {
        url: '/setings',
        views: {
          'pageContent': {
            templateUrl: 'screen-settings/templates/settings.html',
            controller: 'SettingsCtrl as vm'
          }
        }
      });
  });
