'use strict';

angular
  .module('serviceMocks')
  .service('Mocks', function () {

    return {
      user: {
        avatar: 'component-profile/assets/images/avatar.png',
        name: 'User Name',
        phone: '+79151234567',
        money: 15.99,
        location: 'Украина, Киев',
        contacts: {
          count: 678
        },
        about: "Speed is important. So important that you only notice when it isn't there. Ionic is built to perform and behave great on the latest mobile devices. Designed with web application best practices like efficient virtual DOM rendering, hardware accelerated transitions, and touch-optimized gestures, one thing is for sure: You'll be impressed."
      },
      news: [
        {
          avatar: 'component-item-news/assets/images/avatar.png',
          title: 'Public Name',
          date: '10 Декабря 2016 в 18:45',
          image: 'component-item-news/assets/images/image.jpg',
          description: 'A match made in heaven. Ionic builds on top of Angular to create a powerful SDK well-suited for building rich and robust mobile apps for the app store and the mobile web. Ionic not only looks nice, but its core architecture is built for serious app development. Ionic is modeled off of standard native mobile development SDKs, bringing the UI standards of native apps together with the full power and flexibility of the open web. Ionic runs inside Cordova or Phonegap to deploy natively, or as a Progressive Web App. Develop once, deploy everywhere.',
          likes: 180756,
          reposts: 180756
        },
        {
          avatar: 'component-item-news/assets/images/avatar.png',
          title: 'Public Name',
          date: '10 Декабря 2016 в 18:45',
          description: 'A match made in heaven. Ionic builds on top of Angular to create a powerful SDK well-suited for building rich and robust mobile apps for the app store and the mobile web. Ionic not only looks nice, but its core architecture is built for serious app development. Ionic is modeled off of standard native mobile development SDKs, bringing the UI standards of native apps together with the full power and flexibility of the open web. Ionic runs inside Cordova or Phonegap to deploy natively, or as a Progressive Web App. Develop once, deploy everywhere.',
          likes: 180756,
          reposts: 180756
        },
        {
          avatar: 'component-item-news/assets/images/avatar.png',
          title: 'Public Name',
          date: '10 Декабря 2016 в 18:45',
          image: 'component-item-news/assets/images/image.jpg',
          description: 'A match made in heaven. Ionic builds on top of Angular to create a powerful SDK well-suited for building rich and robust mobile apps for the app store and the mobile web. Ionic not only looks nice, but its core architecture is built for serious app development. Ionic is modeled off of standard native mobile development SDKs, bringing the UI standards of native apps together with the full power and flexibility of the open web. Ionic runs inside Cordova or Phonegap to deploy natively, or as a Progressive Web App. Develop once, deploy everywhere.',
          likes: 180756,
          reposts: 180756
        },
        {
          avatar: 'component-item-news/assets/images/avatar.png',
          title: 'Public Name',
          date: '10 Декабря 2016 в 18:45',
          image: 'component-item-news/assets/images/image.jpg',
          description: 'A match made in heaven. Ionic builds on top of Angular to create a powerful SDK well-suited for building rich and robust mobile apps for the app store and the mobile web. Ionic not only looks nice, but its core architecture is built for serious app development. Ionic is modeled off of standard native mobile development SDKs, bringing the UI standards of native apps together with the full power and flexibility of the open web. Ionic runs inside Cordova or Phonegap to deploy natively, or as a Progressive Web App. Develop once, deploy everywhere.',
          likes: 180756,
          reposts: 180756
        },
        {
          avatar: 'component-item-news/assets/images/avatar.png',
          title: 'Public Name',
          date: '10 Декабря 2016 в 18:45',
          image: 'component-item-news/assets/images/image.jpg',
          description: 'A match made in heaven. Ionic builds on top of Angular to create a powerful SDK well-suited for building rich and robust mobile apps for the app store and the mobile web. Ionic not only looks nice, but its core architecture is built for serious app development. Ionic is modeled off of standard native mobile development SDKs, bringing the UI standards of native apps together with the full power and flexibility of the open web. Ionic runs inside Cordova or Phonegap to deploy natively, or as a Progressive Web App. Develop once, deploy everywhere.',
          likes: 180756,
          reposts: 180756
        }
      ],
      rooms: [
        {
          date: 'Сегодня',
          rooms: [
            {
              id: 0,
              last_message: {
                avatar: 'screen-tab-main-rooms/assets/images/avatar01.png',
                name: 'User Name',
                time: '13:43',
                new: true,
                message: 'Ionic is modeled off of standard native mobile development SDKs, bringing the UI standards of native apps together with the full power and flexibility of the open web. Ionic runs inside Cordova or Phonegap to deploy natively, or as a Progressive Web App. Develop once, deploy everywhere.'
              }
            },
            {
              id: 0,
              last_message: {
                avatar: 'screen-tab-main-rooms/assets/images/avatar02.png',
                name: 'User Name',
                time: '13:43',
                message: 'Ionic is modeled off of standard native mobile development SDKs, bringing the UI standards of native apps together with the full power and flexibility of the open web. Ionic runs inside Cordova or Phonegap to deploy natively, or as a Progressive Web App. Develop once, deploy everywhere.'
              }
            },
            {
              id: 0,
              last_message: {
                avatar: 'screen-tab-main-rooms/assets/images/avatar04.png',
                name: 'User Name',
                time: '13:43',
                message: 'Ionic is modeled off of standard native mobile development SDKs, bringing the UI standards of native apps together with the full power and flexibility of the open web. Ionic runs inside Cordova or Phonegap to deploy natively, or as a Progressive Web App. Develop once, deploy everywhere.'
              }
            },
            {
              id: 1,
              group: {
                avatar: 'screen-tab-main-rooms/assets/images/avatar03.png',
                title: 'Group Name',
                count: 645
              },
              last_message: {
                avatar: 'screen-tab-main-rooms/assets/images/avatar01.png',
                name: 'User Name',
                time: '13:43',
                message: 'Ionic is modeled off of standard native mobile development SDKs, bringing the UI standards of native apps together with the full power and flexibility of the open web. Ionic runs inside Cordova or Phonegap to deploy natively, or as a Progressive Web App. Develop once, deploy everywhere.'
              }
            }
          ]
        },
        {
          date: 'Вчера',
          rooms: [
            {
              id: 0,
              last_message: {
                avatar: 'screen-tab-main-rooms/assets/images/avatar.png',
                name: 'User Name',
                time: '13:43',
                message: 'Ionic is modeled off of standard native mobile development SDKs, bringing the UI standards of native apps together with the full power and flexibility of the open web. Ionic runs inside Cordova or Phonegap to deploy natively, or as a Progressive Web App. Develop once, deploy everywhere.'
              }
            },
            {
              id: 0,
              last_message: {
                avatar: 'screen-tab-main-rooms/assets/images/avatar04.png',
                name: 'User Name',
                time: '13:43',
                message: 'Ionic is modeled off of standard native mobile development SDKs, bringing the UI standards of native apps together with the full power and flexibility of the open web. Ionic runs inside Cordova or Phonegap to deploy natively, or as a Progressive Web App. Develop once, deploy everywhere.'
              }
            },
            {
              id: 0,
              last_message: {
                avatar: 'screen-tab-main-rooms/assets/images/avatar01.png',
                name: 'User Name',
                time: '13:43',
                message: 'Ionic is modeled off of standard native mobile development SDKs, bringing the UI standards of native apps together with the full power and flexibility of the open web. Ionic runs inside Cordova or Phonegap to deploy natively, or as a Progressive Web App. Develop once, deploy everywhere.'
              }
            }
          ]
        }
      ],
      chat: [
        {
          id: 0,
          avatar: 'screen-tab-main-rooms/assets/images/avatar01.png',
          name: 'User Name',
          phone: '+79501234567',
          messages: [
            {
              user: 0,
              content: {
                message: "Hi"
              },
              time: '12:45',
              status: 'send'
            },
            {
              user: 1,
              content: {
                message: "Hello"
              },
              time: '12:45',
              status: 'send'
            },
            {
              user: 0,
              content: {
                message: 'Clean, simple, and functional. Ionic has been designed to work and display beautifully on all current mobile devices and platforms.'
              },
              time: '12:45',
              status: 'read'
            },
            {
              user: 1,
              content: {
                message: "With ready-made mobile components, typography, and a gorgeous (yet extensible) base theme that adapts to each platform, you'll be building in style."
              },
              time: '18:45',
              status: 'read'
            },
            {
              user: 0,
              content: {
                image: 'screen-chat/assets/images/image.jpg'
              },
              time: '12:45',
              status: 'read'
            },
            {
              user: 1,
              content: {
                message: 'Use just one command to create, build, test, and deploy your Ionic apps onto any platform.'
              },
              time: '18:45',
              status: 'send'
            },
            {
              user: 0,
              content: {
                message: "With amazing features like Live Reload and integrated logging, you'll already be miles ahead of your native frenemies."
              },
              time: '12:45',
              status: 'send'
            }
          ]
        },
        {
          id: 1,
          avatar: 'screen-tab-main-rooms/assets/images/avatar01.png',
          name: 'User Name',
          phone: '+79501234567',
          group: {
            title: 'Group Name',
            count: 1050
          },
          messages: [
            {
              user: 0,
              group: true,
              content: {
                message: "Hi"
              },
              time: '12:45',
              status: 'send'
            },
            {
              user: 1,
              group: true,
              content: {
                message: "Hello"
              },
              time: '12:45',
              status: 'send'
            },
            {
              user: 0,
              group: true,
              content: {
                message: 'Clean, simple, and functional. Ionic has been designed to work and display beautifully on all current mobile devices and platforms.'
              },
              time: '12:45',
              status: 'read'
            },
            {
              user: 1,
              group: true,
              content: {
                message: "With ready-made mobile components, typography, and a gorgeous (yet extensible) base theme that adapts to each platform, you'll be building in style."
              },
              time: '18:45',
              status: 'read'
            },
            {
              user: 0,
              group: true,
              content: {
                image: 'screen-chat/assets/images/image.jpg'
              },
              time: '12:45',
              status: 'read'
            },
            {
              user: 1,
              group: true,
              content: {
                message: 'Use just one command to create, build, test, and deploy your Ionic apps onto any platform.'
              },
              time: '18:45',
              status: 'send'
            },
            {
              user: 0,
              group: true,
              content: {
                message: "With amazing features like Live Reload and integrated logging, you'll already be miles ahead of your native frenemies."
              },
              time: '12:45',
              status: 'send'
            }
          ]
        }
      ],
      users: [
        {
          avatar: 'screen-tab-main-rooms/assets/images/avatar01.png',
          name: 'User Name',
          phone: '+79151234567'
        },
        {
          avatar: 'screen-tab-main-rooms/assets/images/avatar02.png',
          name: 'User Name',
          phone: '+79151234567'
        },
        {
          avatar: 'screen-tab-main-rooms/assets/images/avatar03.png',
          name: 'User Name',
          phone: '+79151234567'
        },
        {
          avatar: 'screen-tab-main-rooms/assets/images/avatar04.png',
          name: 'User Name',
          phone: '+79151234567'
        },
        {
          avatar: 'screen-tab-main-rooms/assets/images/avatar.png',
          name: 'User Name',
          phone: '+79151234567'
        },
        {
          avatar: 'screen-tab-main-rooms/assets/images/avatar01.png',
          name: 'User Name',
          phone: '+79151234567'
        },
        {
          avatar: 'screen-tab-main-rooms/assets/images/avatar02.png',
          name: 'User Name',
          phone: '+79151234567'
        },
        {
          avatar: 'screen-tab-main-rooms/assets/images/avatar03.png',
          name: 'User Name',
          phone: '+79151234567'
        },
        {
          avatar: 'screen-tab-main-rooms/assets/images/avatar04.png',
          name: 'User Name',
          phone: '+79151234567'
        },
        {
          avatar: 'screen-tab-main-rooms/assets/images/avatar.png',
          name: 'User Name',
          phone: '+79151234567'
        }
      ],
      publics: {
        populars: [
          {
            title: 'Need for speed',
            subtitle: 'Официальные новости',
            background: 'component-item-public/assets/images/public1.jpg',
            count_users: '118.7k',
            likes: '980.2k',
            description: 'Ionic automatically takes platform configurations into account to adjust things like what transition style to use and whether tab icons should show on the top or bottom. For example, iOS will move forward by transitioning the entering view from right to center and the leaving view from center to left. However, Android will transition with the entering view going from bottom to center, covering the previous view, which remains stationary. It should be noted that when a platform is not iOS or Android, then it’ll default to iOS. So if you are developing on a desktop browser, it’s going to take on iOS default configs.',
            author: {
              avatar: 'screen-tab-main-rooms/assets/images/avatar01.png',
              name: 'User Name',
              phone: '+79501234567'
            }
          },
          {
            title: 'Call of duty',
            subtitle: 'Black ops',
            background: 'component-item-public/assets/images/public2.jpg',
            count_users: '118.7k',
            likes: '980.2k',
            description: 'Ionic automatically takes platform configurations into account to adjust things like what transition style to use and whether tab icons should show on the top or bottom. For example, iOS will move forward by transitioning the entering view from right to center and the leaving view from center to left. However, Android will transition with the entering view going from bottom to center, covering the previous view, which remains stationary. It should be noted that when a platform is not iOS or Android, then it’ll default to iOS. So if you are developing on a desktop browser, it’s going to take on iOS default configs.',
            author: {
              avatar: 'screen-tab-main-rooms/assets/images/avatar02.png',
              name: 'User Name',
              phone: '+79501234567'
            }
          },
          {
            title: 'DOTA 2',
            subtitle: 'Официальный фан-клуб',
            background: 'component-item-public/assets/images/public3.jpg',
            count_users: '118.7k',
            likes: '980.2k',
            description: 'Ionic automatically takes platform configurations into account to adjust things like what transition style to use and whether tab icons should show on the top or bottom. For example, iOS will move forward by transitioning the entering view from right to center and the leaving view from center to left. However, Android will transition with the entering view going from bottom to center, covering the previous view, which remains stationary. It should be noted that when a platform is not iOS or Android, then it’ll default to iOS. So if you are developing on a desktop browser, it’s going to take on iOS default configs.',
            author: {
              avatar: 'screen-tab-main-rooms/assets/images/avatar03.png',
              name: 'User Name',
              phone: '+79501234567'
            }
          },
          {
            title: 'World of Tanks',
            subtitle: 'Официальные новости',
            background: 'component-item-public/assets/images/public4.jpg',
            count_users: '118.7k',
            likes: '980.2k',
            description: 'Ionic automatically takes platform configurations into account to adjust things like what transition style to use and whether tab icons should show on the top or bottom. For example, iOS will move forward by transitioning the entering view from right to center and the leaving view from center to left. However, Android will transition with the entering view going from bottom to center, covering the previous view, which remains stationary. It should be noted that when a platform is not iOS or Android, then it’ll default to iOS. So if you are developing on a desktop browser, it’s going to take on iOS default configs.',
            author: {
              avatar: 'screen-tab-main-rooms/assets/images/avatar04.png',
              name: 'User Name',
              phone: '+79501234567'
            }
          },
          {
            title: 'Final Fantasy',
            subtitle: 'Официальный фан-клуб',
            background: 'component-item-public/assets/images/public5.jpg',
            count_users: '118.7k',
            likes: '980.2k',
            description: 'Ionic automatically takes platform configurations into account to adjust things like what transition style to use and whether tab icons should show on the top or bottom. For example, iOS will move forward by transitioning the entering view from right to center and the leaving view from center to left. However, Android will transition with the entering view going from bottom to center, covering the previous view, which remains stationary. It should be noted that when a platform is not iOS or Android, then it’ll default to iOS. So if you are developing on a desktop browser, it’s going to take on iOS default configs.',
            author: {
              avatar: 'screen-tab-main-rooms/assets/images/avatar.png',
              name: 'User Name',
              phone: '+79501234567'
            }
          },
          {
            title: 'Assassins Creed',
            subtitle: 'Официальные новости',
            background: 'component-item-public/assets/images/public6.jpg',
            count_users: '118.7k',
            likes: '980.2k',
            description: 'Ionic automatically takes platform configurations into account to adjust things like what transition style to use and whether tab icons should show on the top or bottom. For example, iOS will move forward by transitioning the entering view from right to center and the leaving view from center to left. However, Android will transition with the entering view going from bottom to center, covering the previous view, which remains stationary. It should be noted that when a platform is not iOS or Android, then it’ll default to iOS. So if you are developing on a desktop browser, it’s going to take on iOS default configs.',
            author: {
              avatar: 'screen-tab-main-rooms/assets/images/avatar01.png',
              name: 'User Name',
              phone: '+79501234567'
            }
          }
        ],
        subscriptions: [
          {
            title: 'Need for speed',
            subtitle: 'Официальные новости',
            background: 'component-item-public/assets/images/public1.jpg',
            count_users: '118.7k',
            likes: '980.2k',
            description: 'Ionic automatically takes platform configurations into account to adjust things like what transition style to use and whether tab icons should show on the top or bottom. For example, iOS will move forward by transitioning the entering view from right to center and the leaving view from center to left. However, Android will transition with the entering view going from bottom to center, covering the previous view, which remains stationary. It should be noted that when a platform is not iOS or Android, then it’ll default to iOS. So if you are developing on a desktop browser, it’s going to take on iOS default configs.',
            author: {
              avatar: 'screen-tab-main-rooms/assets/images/avatar01.png',
              name: 'User Name',
              phone: '+79501234567'
            }
          },
          {
            title: 'Call of duty',
            subtitle: 'Black ops',
            background: 'component-item-public/assets/images/public2.jpg',
            count_users: '118.7k',
            likes: '980.2k',
            description: 'Ionic automatically takes platform configurations into account to adjust things like what transition style to use and whether tab icons should show on the top or bottom. For example, iOS will move forward by transitioning the entering view from right to center and the leaving view from center to left. However, Android will transition with the entering view going from bottom to center, covering the previous view, which remains stationary. It should be noted that when a platform is not iOS or Android, then it’ll default to iOS. So if you are developing on a desktop browser, it’s going to take on iOS default configs.',
            author: {
              avatar: 'screen-tab-main-rooms/assets/images/avatar02.png',
              name: 'User Name',
              phone: '+79501234567'
            }
          },
          {
            title: 'DOTA 2',
            subtitle: 'Официальный фан-клуб',
            background: 'component-item-public/assets/images/public3.jpg',
            count_users: '118.7k',
            likes: '980.2k',
            description: 'Ionic automatically takes platform configurations into account to adjust things like what transition style to use and whether tab icons should show on the top or bottom. For example, iOS will move forward by transitioning the entering view from right to center and the leaving view from center to left. However, Android will transition with the entering view going from bottom to center, covering the previous view, which remains stationary. It should be noted that when a platform is not iOS or Android, then it’ll default to iOS. So if you are developing on a desktop browser, it’s going to take on iOS default configs.',
            author: {
              avatar: 'screen-tab-main-rooms/assets/images/avatar03.png',
              name: 'User Name',
              phone: '+79501234567'
            }
          },
          {
            title: 'World of Tanks',
            subtitle: 'Официальные новости',
            background: 'component-item-public/assets/images/public4.jpg',
            count_users: '118.7k',
            likes: '980.2k',
            description: 'Ionic automatically takes platform configurations into account to adjust things like what transition style to use and whether tab icons should show on the top or bottom. For example, iOS will move forward by transitioning the entering view from right to center and the leaving view from center to left. However, Android will transition with the entering view going from bottom to center, covering the previous view, which remains stationary. It should be noted that when a platform is not iOS or Android, then it’ll default to iOS. So if you are developing on a desktop browser, it’s going to take on iOS default configs.',
            author: {
              avatar: 'screen-tab-main-rooms/assets/images/avatar04.png',
              name: 'User Name',
              phone: '+79501234567'
            }
          },
          {
            title: 'Final Fantasy',
            subtitle: 'Официальный фан-клуб',
            background: 'component-item-public/assets/images/public5.jpg',
            count_users: '118.7k',
            likes: '980.2k',
            description: 'Ionic automatically takes platform configurations into account to adjust things like what transition style to use and whether tab icons should show on the top or bottom. For example, iOS will move forward by transitioning the entering view from right to center and the leaving view from center to left. However, Android will transition with the entering view going from bottom to center, covering the previous view, which remains stationary. It should be noted that when a platform is not iOS or Android, then it’ll default to iOS. So if you are developing on a desktop browser, it’s going to take on iOS default configs.',
            author: {
              avatar: 'screen-tab-main-rooms/assets/images/avatar.png',
              name: 'User Name',
              phone: '+79501234567'
            }
          },
          {
            title: 'Assassins Creed',
            subtitle: 'Официальные новости',
            background: 'component-item-public/assets/images/public6.jpg',
            count_users: '118.7k',
            likes: '980.2k',
            description: 'Ionic automatically takes platform configurations into account to adjust things like what transition style to use and whether tab icons should show on the top or bottom. For example, iOS will move forward by transitioning the entering view from right to center and the leaving view from center to left. However, Android will transition with the entering view going from bottom to center, covering the previous view, which remains stationary. It should be noted that when a platform is not iOS or Android, then it’ll default to iOS. So if you are developing on a desktop browser, it’s going to take on iOS default configs.',
            author: {
              avatar: 'screen-tab-main-rooms/assets/images/avatar01.png',
              name: 'User Name',
              phone: '+79501234567'
            }
          }
        ],
        my: [
          {
            title: 'Need for speed',
            subtitle: 'Официальные новости',
            background: 'component-item-public/assets/images/public1.jpg',
            count_users: '118.7k',
            likes: '980.2k',
            description: 'Ionic automatically takes platform configurations into account to adjust things like what transition style to use and whether tab icons should show on the top or bottom. For example, iOS will move forward by transitioning the entering view from right to center and the leaving view from center to left. However, Android will transition with the entering view going from bottom to center, covering the previous view, which remains stationary. It should be noted that when a platform is not iOS or Android, then it’ll default to iOS. So if you are developing on a desktop browser, it’s going to take on iOS default configs.',
            author: {
              avatar: 'screen-tab-main-rooms/assets/images/avatar01.png',
              name: 'User Name',
              phone: '+79501234567'
            }
          }
        ]
      },
      shop: [
        {
          cover: 'component-item-shop/assets/images/shop1.jpg',
          title: 'Title',
          price: '0.99'
        },
        {
          cover: 'component-item-shop/assets/images/shop2.jpg',
          title: 'Title',
          price: '0.99'
        },
        {
          cover: 'component-item-shop/assets/images/shop3.jpg',
          title: 'Title',
          price: '0.99'
        }
      ]
    }
  });
