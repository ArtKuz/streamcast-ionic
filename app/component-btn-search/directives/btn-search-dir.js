'use strict';

angular
  .module('componentBtnSearch')
  .directive('btnSearch', function () {
    return {
      restrict: 'E',
      templateUrl: 'component-btn-search/templates/btn-search.html',
      controllerAs: 'vm',
      scope: {},
      bindToController: true,
      controller: function($ionicModal,
                           $scope) {
        var vm = this;

        $ionicModal.fromTemplateUrl('component-btn-search/templates/modal-search.html', {
          scope: $scope,
          animation: 'slide-in-up',
          focusFirstInput: true
        })
        .then(function (modal) {
          vm.modal = modal;
        });

        vm.openModal = function() {
          vm.modal.show();
        };

        vm.closeModal = function() {
          vm.modal.hide();
        };

        vm.destroyModal = function() {
          vm.modal.remove();
        };

        $scope.$on('$destroy', function() {
          vm.modal.remove();
        });

      }
    };
  });
