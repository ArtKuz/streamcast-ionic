'use strict';

angular
  .module('screenTabsGroupChat', [])
  .config(function($ionicConfigProvider) {
    $ionicConfigProvider.tabs.position('top');
    $ionicConfigProvider.tabs.style('striped');
  })
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.group-chat', {
        url: '/group-chat/:group_chat_id',
        abstract: true,
        views: {
          'pageContent': {
            templateUrl: "screen-tabs-group-chat/templates/tabs-group-chat.html"
          }
        }
      })
  });
