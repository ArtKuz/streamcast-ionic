'use strict';

angular
  .module('componentBtnAddUser')
  .directive('btnAddUser', function () {
    return {
      restrict: 'E',
      templateUrl: 'component-btn-add-user/templates/btn-add-user.html',
      controllerAs: 'vm',
      scope: {},
      bindToController: true,
      controller: function() {
        var vm = this;

      }
    };
  });
