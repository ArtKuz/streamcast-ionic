'use strict';

angular
  .module('screenTabsMain', [])
  .config(function($ionicConfigProvider) {
    $ionicConfigProvider.tabs.position('top');
    $ionicConfigProvider.tabs.style('striped');
  })
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.tabs', {
        url: '/tabs',
        abstract: true,
        views: {
          'pageContent': {
            templateUrl: "screen-tabs-main/templates/tabs-main.html"
          }
        }
      })
  });
