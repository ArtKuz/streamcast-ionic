'use strict';

angular
  .module('componentBtnMoreSettings')
  .directive('btnMoreSettings', function () {
    return {
      restrict: 'E',
      templateUrl: 'component-btn-more-settings/templates/btn-more-settings.html',
      controllerAs: 'vm',
      scope: {},
      bindToController: true,
      controller: function($mdBottomSheet,
                           $scope) {
        var vm = this;

        vm.showListBottomSheet = function($event) {
          $mdBottomSheet.show({
            templateUrl: 'component-btn-more-settings/templates/sheet-more-settings.html',
            targetEvent: $event,
            scope: $scope.$new(false)
          });
        };

        $scope.closeListBottomSheet = function () {
          $mdBottomSheet.hide();
        }
      }
    };
  });
