'use strict';

angular
  .module('screenTabGroupChatMembers')
  .controller('TabGroupChatMembersCtrl', function (Mocks) {
    var vm = this;

    vm.users = Mocks.users;
  });
