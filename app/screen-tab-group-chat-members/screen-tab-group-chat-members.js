'use strict';

angular
  .module('screenTabGroupChatMembers', [
    'componentItemUserControl',
    'componentBtnMoreSettings',
    'componentBtnSearch'
  ])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.group-chat-members', {
        url: '/group-chat-members/:group_chat_id',
        views: {
          'pageContent': {
            templateUrl: "screen-tab-group-chat-members/templates/tab-group-chat-members.html",
            controller: 'TabGroupChatMembersCtrl as vm'
          }
        }
      })
  });
