'use strict';

angular
  .module('componentItemNews')
  .directive('itemNews', function () {
    return {
      restrict: 'E',
      templateUrl: 'component-item-news/templates/item-news.html',
      controllerAs: 'vm',
      scope: {
        news: '='
      },
      bindToController: true,
      controller: function() {
        var vm = this;

        vm.statusNews = false;

        vm.read = function() {
          vm.statusNews = !vm.statusNews;
        }
      }
    };
  });
