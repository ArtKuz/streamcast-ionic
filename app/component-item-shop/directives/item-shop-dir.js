'use strict';

angular
  .module('componentItemShop')
  .directive('itemShop', function () {
    return {
      restrict: 'E',
      templateUrl: 'component-item-shop/templates/item-shop.html',
      controllerAs: 'vm',
      scope: {
        item: '='
      },
      bindToController: true,
      controller: function() {
        var vm = this;

      }
    };
  });
