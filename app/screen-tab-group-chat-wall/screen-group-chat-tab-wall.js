'use strict';

angular
  .module('screenTabGroupChatWall', [
    'componentMessage',
    'componentBtnClip',
    'componentSendMessage',
    'componentSendSticker',
    'componentBtnMoreSettings',
    'componentBtnAddUser'
  ])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.group-chat.wall', {
        url: "/wall",
        views: {
          'tab-group-chat-wall': {
            templateUrl: "screen-tab-group-chat-wall/templates/tab-group-chat-wall.html",
            controller: 'TabGroupChatWallCtrl as vm'
          }
        }
      })
  });
