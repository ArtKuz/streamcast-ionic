'use strict';

angular
  .module('screenTabPublicsSubscription')
  .controller('TabPublicsSubscriptionCtrl', function (Mocks) {
    var vm = this;

    vm.populars = Mocks.publics.subscriptions;
  });
