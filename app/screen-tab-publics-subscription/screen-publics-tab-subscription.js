'use strict';

angular
  .module('screenTabPublicsSubscription', [
    'componentItemPublic',
    'componentBtnSearch'
  ])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.publics.subscription', {
        url: "/subscription",
        views: {
          'tab-publics-subscription': {
            templateUrl: "screen-tab-publics-subscription/templates/tab-publics-subscription.html",
            controller: 'TabPublicsSubscriptionCtrl as vm'
          }
        }
      })
  });
