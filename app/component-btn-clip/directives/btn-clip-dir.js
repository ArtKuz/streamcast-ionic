'use strict';

angular
  .module('componentBtnClip')
  .directive('btnClip', function () {
    return {
      restrict: 'E',
      templateUrl: 'component-btn-clip/templates/btn-clip.html',
      controllerAs: 'vm',
      scope: {},
      bindToController: true,
      controller: function($mdBottomSheet,
                           $scope) {
        var vm = this;

        vm.showListBottomSheet = function($event) {
          $mdBottomSheet.show({
            templateUrl: 'component-btn-clip/templates/btn-clip-sheet.html',
            targetEvent: $event,
            scope: $scope.$new(false)
          });
        };

        $scope.closeListBottomSheet = function () {
          $mdBottomSheet.hide();
        }
      }
    };
  });
