'use strict';

angular
  .module('screenTabPublicPublics', [
    'componentBtnSearch',
    'componentBtnMoreSettings'
  ])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.public.publics', {
        url: "/publics",
        views: {
          'tab-public-publics': {
            templateUrl: "screen-tab-public-publics/templates/tab-public-publics.html",
            controller: 'TabPublicPublicsCtrl as vm'
          }
        }
      })
  });
