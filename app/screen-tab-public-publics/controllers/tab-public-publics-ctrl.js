'use strict';

angular
  .module('screenTabPublicPublics')
  .controller('TabPublicPublicsCtrl', function (Mocks) {
    var vm = this;

    vm.public = Mocks.publics.populars[0];
    vm.newsList = Mocks.news;
  });
