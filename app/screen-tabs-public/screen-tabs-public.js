'use strict';

angular
  .module('screenTabsPublic', [])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.public', {
        url: '/public/:public_id',
        abstract: true,
        views: {
          'pageContent': {
            templateUrl: "screen-tabs-public/templates/tabs-public.html"
          }
        }
      })
  });
