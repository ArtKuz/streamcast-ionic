'use strict';

angular
  .module('componentBtnChatUser')
  .directive('btnChatUser', function () {
    return {
      restrict: 'E',
      templateUrl: 'component-btn-chat-user/templates/btn-chat-user.html',
      controllerAs: 'vm',
      scope: {},
      bindToController: true,
      controller: function() {
        var vm = this;

      }
    };
  });
