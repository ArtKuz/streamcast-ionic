'use strict';

angular
  .module('componentProfile', [
    'ionic',
    'ngCordova',
    'ui.router'
  ]);
