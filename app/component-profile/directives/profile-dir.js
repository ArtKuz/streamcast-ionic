'use strict';

angular
  .module('componentProfile')
  .directive('profile', function ($http,
                                  $compile,
                                  $templateCache) {
    return {
      restrict: 'E',
      controllerAs: 'profile',
      scope: {
        type: '@',
        user: '='
      },
      bindToController: true,
      link: function postLink(scope, element, attrs, controller) {
        (function() {
          $http.get('component-profile/templates/profile-' + controller.type + '.html', { cache: $templateCache })
            .success(function (tplContent) {
              element.html(tplContent);
              $compile(element.contents())(scope);
            });
        })();
      },
      controller: function() {
        var profile = this;
      }
    };
  });
