'use strict';

angular
  .module('screenTabMainNews', [
    'serviceMocks',
    'componentItemNews',
    'componentBtnSearch'
  ])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.tabs.news', {
        url: "/news",
        views: {
          'tab-news': {
            templateUrl: "screen-tab-main-news/templates/tab-main-news.html",
            controller: 'TabMainNewsCtrl as vm'
          }
        }
      })
  });
