'use strict';

angular
  .module('screenTabMainNews')
  .controller('TabMainNewsCtrl', function (Mocks) {
    var vm = this;

    vm.newsList = Mocks.news;
  });
