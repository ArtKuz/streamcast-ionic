'use strict';

angular
  .module('componentBtnReply')
  .directive('btnReply', function () {
    return {
      restrict: 'E',
      templateUrl: 'component-btn-reply/templates/btn-reply.html',
      controllerAs: 'vm',
      scope: {},
      bindToController: true,
      controller: function() {
        var vm = this;

      }
    };
  });
