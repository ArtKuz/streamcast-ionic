'use strict';

angular
  .module('screenTabPublicsPopular')
  .controller('TabPublicsPopularCtrl', function (Mocks) {
    var vm = this;

    vm.populars = Mocks.publics.populars;
  });
