'use strict';

angular
  .module('screenTabPublicsPopular', [
    'componentItemPublic',
    'componentBtnSearch'
  ])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.publics.popular', {
        url: "/popular",
        views: {
          'tab-publics-popular': {
            templateUrl: "screen-tab-publics-popular/templates/tab-publics-popular.html",
            controller: 'TabPublicsPopularCtrl as vm'
          }
        }
      })
  });
